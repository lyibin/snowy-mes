import { axios } from '@/utils/request'

/**
 * 查询生产计划
 *
 * @author 嘉欣
 * @date 2022-07-19 11:40:28
 */
export function proPlanPage (parameter) {
  return axios({
    url: '/proPlan/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 生产计划列表
 *
 * @author 嘉欣
 * @date 2022-07-19 11:40:28
 */
export function proPlanList (parameter) {
  return axios({
    url: '/proPlan/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加生产计划
 *
 * @author 嘉欣
 * @date 2022-07-19 11:40:28
 */
export function proPlanAdd (parameter) {
  return axios({
    url: '/proPlan/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑生产计划
 *
 * @author 嘉欣
 * @date 2022-07-19 11:40:28
 */
export function proPlanEdit (parameter) {
  return axios({
    url: '/proPlan/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除生产计划
 *
 * @author 嘉欣
 * @date 2022-07-19 11:40:28
 */
export function proPlanDelete (parameter) {
  return axios({
    url: '/proPlan/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出生产计划
 *
 * @author 嘉欣
 * @date 2022-07-19 11:40:28
 */
export function proPlanExport (parameter) {
  return axios({
    url: '/proPlan/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
