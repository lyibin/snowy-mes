/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
*/
package vip.xiaonuo.modular.task.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.zstandard.ZstdCompressorOutputStream;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.context.login.LoginContextHolder;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.login.SysLoginUser;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.pro.entity.Pro;
import vip.xiaonuo.modular.pro.service.ProService;
import vip.xiaonuo.modular.promodel.enums.ProModelExceptionEnum;
import vip.xiaonuo.modular.task.entity.Task;
import vip.xiaonuo.modular.task.enums.TaskExceptionEnum;
import vip.xiaonuo.modular.task.enums.TaskStatusEnum;
import vip.xiaonuo.modular.task.mapper.TaskMapper;
import vip.xiaonuo.modular.task.param.TaskParam;
import vip.xiaonuo.modular.task.result.TaskResult;
import vip.xiaonuo.modular.task.service.TaskService;
import vip.xiaonuo.modular.workorder.entity.WorkOrder;
import vip.xiaonuo.modular.workorder.enums.WorkOrderStatusEnum;
import vip.xiaonuo.modular.workorder.service.WorkOrderService;
import vip.xiaonuo.modular.workreport.entity.WorkReport;
import vip.xiaonuo.modular.workreport.enums.WorkReportApprovalEnum;
import vip.xiaonuo.modular.workreport.enums.WorkReportExceptionEnum;
import vip.xiaonuo.modular.workreport.service.WorkReportService;
import vip.xiaonuo.sys.core.enums.AdminTypeEnum;


import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 任务service接口实现类
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
@Service
@Slf4j
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements TaskService {
    @Resource
    private WorkReportService workReportService;
    @Resource
    private ProService proService;
    @Autowired
    TaskMapper taskMapper;
    @Resource
    WorkOrderService workOrdertService;

    @Override
    public PageResult<TaskResult> page(TaskParam taskParam) {
        //调用封装方法
        QueryWrapper<TaskResult> queryWrapper = getqueryWrapper(taskParam);
        return new PageResult<>(this.baseMapper.page(PageFactory.defaultPage(), queryWrapper));
    }

    /**
     * 封装查询条件
     */
    private QueryWrapper<TaskResult> getqueryWrapper(TaskParam taskParam) {
        QueryWrapper<TaskResult> queryWrapper = new QueryWrapper<>();
        // 当前用户
        SysLoginUser sysLoginUser = LoginContextHolder.me().getSysLoginUser();
        // 判断当前用户是否为超级管理员
        if(!AdminTypeEnum.SUPER_ADMIN.getCode().equals(sysLoginUser.getAdminType())){
            Long loginUserId = LoginContextHolder.me().getSysLoginUser().getId();
            queryWrapper.nested(item-> {
                        item.lambda().apply("instr(concat(';',a.report_right,';'),concat(';',{0},';')) or a.report_right is null ", loginUserId);
                    }
            );
        }

        if (ObjectUtil.isNotNull(taskParam)) {

            // 根据不良品数 查询
            if (ObjectUtil.isNotEmpty(taskParam.getBadNum())) {
                queryWrapper.like("a.bad_num", taskParam.getBadNum());
            }
            // 根据良品数 查询
            if (ObjectUtil.isNotEmpty(taskParam.getGoodNum())) {
                queryWrapper.like("a.good_num", taskParam.getGoodNum());
            }

            if (ObjectUtil.isNotEmpty(taskParam.getStartEndTime())) {

                String str = taskParam.getStartEndTime();
                String[] strArray = str.split(";");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                try {
                    date = sdf.parse(strArray[1]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //得到日历
                Calendar calendar = Calendar.getInstance();
                //把当前时间赋给日历
                calendar.setTime(date);
                queryWrapper.ge("a.pla_start_time", strArray[0]);
                //设置为后一天
                calendar.add(Calendar.DAY_OF_MONTH, +1);
                queryWrapper.lt("a.pla_end_time", sdf.format(calendar.getTime()));
            }
            if (ObjectUtil.isNotEmpty(taskParam.getFactTime())) {

                String str = taskParam.getFactTime();
                String[] strArray = str.split(";");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                try {
                    date = sdf.parse(strArray[1]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance();
                //把当前时间赋给日历
                calendar.setTime(date);
                //设置为后一天
                calendar.add(Calendar.DAY_OF_MONTH, +1);
                //查询条件时间大于等于实际开始时间小于实际结束时间或大于实际开始时间
                queryWrapper.nested(item -> item.ge("a.fact_sta_time", strArray[0]).lt("a.fact_end_time", sdf.format(calendar.getTime())));
                queryWrapper.or().ge("a.fact_sta_time", strArray[0]);
            }
            // 根据计划数 查询
            if (ObjectUtil.isNotEmpty(taskParam.getPlaNum())) {
                queryWrapper.like("a.pla_num", taskParam.getPlaNum());
            }
            // 根据产品id 查询
            if (ObjectUtil.isNotEmpty(taskParam.getProId())) {
                queryWrapper.like("a.pro_id", taskParam.getProId());
            }

            // 根据产品类型id 查询
            if (ObjectUtil.isNotEmpty(taskParam.getProTypeId())) {
                queryWrapper.like("a.pro_type_id", taskParam.getProTypeId());
            }

            // 根据状态 查询
            if (ObjectUtil.isNotEmpty(taskParam.getStatus())) {
                queryWrapper.eq("a.status", taskParam.getStatus());
            }
            // 根据工单id  查询
            if (ObjectUtil.isNotEmpty(taskParam.getWorkOrderId())) {
                queryWrapper.like("a.work_order_id", taskParam.getWorkOrderId());
            }
            // 根据工序id 查询
            if (ObjectUtil.isNotEmpty(taskParam.getWorkStepId())) {
                queryWrapper.like("a.work_step_id", taskParam.getWorkStepId());
            }
            //根据编码查询
            if (ObjectUtil.isNotEmpty(taskParam.getCode())) {
                queryWrapper.like("a.code", taskParam.getCode());
            }

        }
        queryWrapper.orderByDesc("a.create_time");
        return (queryWrapper);

    }

    @Override
    public List<TaskResult> list(TaskParam taskParam) {

        return this.baseMapper.taskList(getqueryWrapper(taskParam));
    }


    @Override
    public void add(TaskParam taskParam) {
        //校验参数，检查是否存在相同的名称和编码
        checkParam(taskParam, false);
        Task task = new Task();
        String[] timeList = taskParam.getStartEndTime().split(";");
        taskParam.setPlaStartTime(timeList[0]);
        taskParam.setPlaEndTime(timeList[1]);
        String[] timesList = taskParam.getFactTime().split(";");
        taskParam.setFactStaTime(timesList[0]);
        taskParam.setFactEndTime(timesList[1]);
        BeanUtil.copyProperties(taskParam, task);
        Pro pro = proService.getById(task.getProId());
        if (pro == null) {
            //找不到产品相应的产品类型，报异常
            throw new ServiceException(ProModelExceptionEnum.NOT_EXIST);
        } else {
            task.setProTypeId(pro.getProTypeId());
            this.save(task);
        }
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<TaskParam> taskParamList) {
        //避免循环调库
        QueryWrapper<WorkReport> workReportQueryWrapper = new QueryWrapper<>();
        QueryWrapper<Task> taskQueryWrapper = new QueryWrapper<>();
        taskParamList.forEach(taskParam -> {
            //同时删掉报工表
            taskQueryWrapper.lambda().eq(Task::getId, taskParam.getId()).or();
            //根据任务id查询报工
            workReportQueryWrapper.lambda().eq(WorkReport::getTaskId, taskParam.getId()).or();

        });
        workReportService.remove(workReportQueryWrapper);
        //执行任务删除
        this.remove(taskQueryWrapper);

//        taskParamList.forEach(taskParam -> {
//            //同时删掉报工表
//            QueryWrapper<WorkReport> queryWrapper = new QueryWrapper<>();
//            queryWrapper.lambda().eq(WorkReport::getTaskId, taskParam.getId());
//            workReportService.remove(queryWrapper);
//            this.removeById(taskParam.getId());
//        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(TaskParam taskParam) {
        //校验参数，检查是否存在相同的名称和编码
        checkParam(taskParam, true);
        Task task = this.queryTask(taskParam);
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        //状态为未开始
        if (TaskStatusEnum.NOT_START.getCode().equals(taskParam.getStatus())) {
            taskParam.setFactStaTime(null);
            taskParam.setFactEndTime(null);
        }
        //状态为执行中
        if (TaskStatusEnum.EXECUTION.getCode().equals(taskParam.getStatus())) {
            if (ObjectUtil.isNotEmpty(taskParam.getFactStaTime())) {
                //结束时间滞空
                taskParam.setFactEndTime(null);
            }
            if (ObjectUtil.isEmpty(taskParam.getFactStaTime())) {
                //把当前时间赋给开始时间
                taskParam.setFactStaTime(dateFormat.format(date));
                //结束时间滞空
                taskParam.setFactEndTime(null);
            }

            //如果实际时间不是空
            if (ObjectUtil.isNotEmpty(taskParam.getFactTime())) {
                String[] factTimesList = taskParam.getFactTime().split(";");
                if (factTimesList.length >= 2) {
                    taskParam.setFactStaTime(factTimesList[0]);
                    //结束时间滞空
                    taskParam.setFactEndTime(null);
                }
            }
        }
        //状态为已结束
        if (TaskStatusEnum.FINISH.getCode().equals(taskParam.getStatus())) {
            //获取任务顺序
            WorkReport workReport = new WorkReport();
            QueryWrapper<Task> taskPreviousQueryWrapper = new QueryWrapper<>();
            //排序
            taskPreviousQueryWrapper.orderByAsc("sort_num");
            taskPreviousQueryWrapper.lambda().eq(Task::getWorkOrderId, task.getWorkOrderId());
            List<Task> taskPreviousSortNum = taskMapper.selectList(taskPreviousQueryWrapper);
            //任务序号判空
            if (ObjectUtil.isEmpty(taskPreviousSortNum)) {
                throw new ServiceException(15, "任务序号为空");
            }

            for (int i = 0; i < taskPreviousSortNum.size(); i++) {
                //判断本任务是否为第一个任务
                if (taskPreviousSortNum.get(0).getSortNum().equals(task.getSortNum())) {
                    break;
                }
                //本次任务的计划数跟上一个任务的良品数进行比较
                if (taskPreviousSortNum.get(i).getSortNum().equals(task.getSortNum())) {
                    if (taskPreviousSortNum.get(i - 1).getGoodNum() < task.getPlaNum()) {

                        throw new ServiceException(16, "上一次任务良品数小于本任务计划数");
                    }
                    //本次任务的良品数大于上次任务的良品数
                    if(taskPreviousSortNum.get(i - 1).getGoodNum() < task.getGoodNum()){
                        throw new ServiceException(17,"上一次任务良品数小于此次良品数");
                    }
                    break;
                }
            }
            //良品数小于计划数生成报工
            if (task.getGoodNum() < task.getPlaNum()) {
                //工单id
                workReport.setWorkOrderId(task.getWorkOrderId());
                //任务id
                workReport.setTaskId(task.getId());
                //生产人员
                workReport.setProductionUser(LoginContextHolder.me().getSysLoginUserId());
                //0从任务添加,1从报工处添加
                workReport.setDistinction(0);
                workReport.setReportNum(task.getPlaNum() - task.getGoodNum());
                workReport.setGoodNum(workReport.getReportNum());
                //审批状态
                workReport.setApproval(WorkReportApprovalEnum.APPROVAL_YES.getCode());
                workReport.setBadNum(0);
                workReport.setStartTime(date);
                workReport.setEndTime(date);
                workReportService.save(workReport);
                taskParam.setGoodNum(task.getPlaNum());
            }
            if (ObjectUtil.isEmpty(taskParam.getFactStaTime())) {
                try {
                    //开始时间取数据库开始时间
                    taskParam.setFactStaTime(dateFormat.format(task.getFactStaTime()));

                } catch (Exception e) {
                    throw new ServiceException(10, "任务未开始，不能结束");
                }

            }
            // 校验 传回来的是否为时间段
            if (ObjectUtil.isNotEmpty(taskParam.getFactTime())) {
                String[] factTimesList = taskParam.getFactTime().split(";");
                if (factTimesList.length >= 2) {
                    taskParam.setFactStaTime(factTimesList[0]);
                    taskParam.setFactEndTime(factTimesList[1]);
                }
            }
            //结束实际结束时间为空
            if (ObjectUtil.isEmpty(taskParam.getFactEndTime())) {
                //实际开始时间为数据库时间
                if (ObjectUtil.isEmpty(taskParam.getFactStaTime())) {
                    taskParam.setFactStaTime(dateFormat.format(task.getFactStaTime()));
                }
                //把当前时间赋给实际结束时间
                taskParam.setFactEndTime(dateFormat.format(date));
            }
        }
        BeanUtil.copyProperties(taskParam, task);
        this.updateById(task);
    }

    @Override
    public Task detail(TaskParam taskParam) {
        Task task = this.queryTask(taskParam);
        String[] timeList = taskParam.getStartEndTime().split(";");
        taskParam.setPlaStartTime(timeList[0]);
        taskParam.setPlaEndTime(timeList[1]);
        String[] timesList = taskParam.getFactTime().split(";");
        taskParam.setFactStaTime(timesList[0]);
        taskParam.setFactEndTime(timesList[1]);
        BeanUtil.copyProperties(taskParam, task);
        return this.queryTask(taskParam);
    }

    /**
     * 获取任务
     *
     * @author bwl
     * @date 2022-06-02 09:27:11
     */
    private Task queryTask(TaskParam taskParam) {
        Task task = this.getById(taskParam.getId());
        if (ObjectUtil.isNull(task)) {
            throw new ServiceException(TaskExceptionEnum.NOT_EXIST);
        }
        return task;
    }

    @Override
    public void export(TaskParam taskParam) {
        List<TaskResult> list = this.list(taskParam);
        PoiUtil.exportExcelWithStream("SnowyTask.xls", Task.class, list);
    }

    /**
     * 开启任务，可批量开始
     *
     * @author bwl
     * @date 2020/6/09 9:59
     */

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void start(List<TaskParam> taskParamList) {
        // 工单状态不为执行中,任务状态不能改变
        List<WorkOrder> workOrderList = workOrdertService.list();
        Map<Long, WorkOrder> workOrderMap = workOrderList.stream().collect(Collectors.toMap(WorkOrder::getId, a -> a,(k1, k2)->k1));
        List<Task> taskList = this.list();
        Map<Long, Task> taskMap = taskList.stream().collect(Collectors.toMap(Task::getId, a -> a,(k1, k2)->k1));
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        List<Task> updateByIdTaskList = new ArrayList<>();
        taskParamList.forEach(taskParam -> {
            if (!WorkOrderStatusEnum.EXECUTION.getCode().equals(workOrderMap.get(taskMap.get(taskParam.getId()).getWorkOrderId()).getStatus())) {
                throw new ServiceException(16, "工单状态不为执行中,任务状态不能改变");
            }
            Task task = this.queryTask(taskParam);
            taskParam.setFactStaTime(dateFormat.format(date));
            //结束时间滞空
            taskParam.setFactEndTime(null);
            taskParam.setStatus(TaskStatusEnum.EXECUTION.getCode());
            BeanUtil.copyProperties(taskParam, task);
            updateByIdTaskList.add(task);
        });
        this.updateBatchById(updateByIdTaskList);

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void end(List<TaskParam> taskParamList) {
        // 创建一个 taskList 需要修改的任务
        List<Task> updateByIdTaskList = new ArrayList<>();
        // 创建一个 workReportList 需要修改的任务
        List<WorkReport> workReportList = new ArrayList<>();
        // 创建一个Map key 为工单id，value为该工单下的所有任务
        QueryWrapper<Task> taskQueryWrapper = new QueryWrapper<>();
        taskQueryWrapper.lambda().orderByAsc(Task::getSortNum);
        List<Task> taskList = this.list(taskQueryWrapper);
        Map<Long,List<Task>> taskWrokOrderMap = taskList.stream().collect(Collectors.groupingBy(Task::getWorkOrderId));
        // 创建一个 Map key 为任务，value为所对应的任务
        Map<Long,Task> taskMap = taskList.stream().collect(Collectors.toMap(Task::getId, a -> a,(k1, k2)->k1));
        // 工单状态不为执行中,任务状态不能改变
        List<WorkOrder> workOrderList = workOrdertService.list();
        Map<Long, WorkOrder> workOrderMap = workOrderList.stream().collect(Collectors.toMap(WorkOrder::getId, a -> a,(k1, k2)->k1));
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        taskParamList.forEach(taskParam -> {
            if (!WorkOrderStatusEnum.EXECUTION.getCode().equals(workOrderMap.get(taskMap.get(taskParam.getId()).getWorkOrderId()).getStatus())) {
                throw new ServiceException(17, "工单状态不为执行中,任务状态不能改变");
            }
            Task task = taskMap.get(taskParam.getId());
            if (ObjectUtil.isNotEmpty(task.getFactStaTime())) {
                taskParam.setFactStaTime(dateFormat.format(task.getFactStaTime()));
            } else {
                taskParam.setFactStaTime(dateFormat.format(date));
            }
            taskParam.setFactEndTime(dateFormat.format(date));
            taskParam.setStatus(TaskStatusEnum.FINISH.getCode());
            //获取任务顺序
            List<Task> taskPreviousSortNum = taskWrokOrderMap.get(task.getWorkOrderId());
            //任务序号判空
            if (ObjectUtil.isEmpty(taskPreviousSortNum)) {
                throw new ServiceException(15, "任务序号为空");
            }
            for (int i = 0; i < taskPreviousSortNum.size(); i++) {
                //判断本任务是否为第一个任务
                if (taskPreviousSortNum.get(0).getSortNum().equals(task.getSortNum())) {
                    break;
                }
                //本次任务的计划数跟上一个任务的良品数进行比较
                if (taskPreviousSortNum.get(i).getSortNum().equals(task.getSortNum())) {
                    if (taskPreviousSortNum.get(i - 1).getGoodNum() < task.getPlaNum()) {

                        throw new ServiceException(16, "上一次任务良品数小于本任务计划数");
                    }
                    //本次任务的良品数大于上次任务的良品数
                    if(taskPreviousSortNum.get(i - 1).getGoodNum() < task.getGoodNum()){
                        throw new ServiceException(17,"上一次任务良品数小于此次良品数");
                    }
                    break;
                }
            }
            //良品数小于计划数生成报工
            if (task.getGoodNum() < task.getPlaNum()) {
                WorkReport workReport = new WorkReport();
                //工单id
                workReport.setWorkOrderId(task.getWorkOrderId());
                //任务id
                workReport.setTaskId(task.getId());
                //生产人员
                workReport.setProductionUser(LoginContextHolder.me().getSysLoginUserId());
                //0从任务添加,1从报工处添加
                workReport.setDistinction(0);
                workReport.setReportNum(task.getPlaNum() - task.getGoodNum());
                workReport.setGoodNum(workReport.getReportNum());
                //审批状态
                workReport.setApproval(WorkReportApprovalEnum.APPROVAL_YES.getCode());
                workReport.setBadNum(0);
                workReport.setStartTime(date);
                workReport.setEndTime(date);
                workReportList.add(workReport);
                taskParam.setGoodNum(task.getPlaNum());
            }
            BeanUtil.copyProperties(taskParam, task);
            updateByIdTaskList.add(task);
        });
        workReportService.saveBatch(workReportList);
        this.updateBatchById(updateByIdTaskList);
    }


    /**
     * 校验参数，检查是否存在相同的名称和编码
     *
     * @author bwl
     * @date 2020/6/09 9:59
     */
    private void checkParam(TaskParam taskParam, boolean isExcludeSelf) {
        String[] startEndTimesList;
        if (ObjectUtil.isNotEmpty(taskParam.getStartEndTime())) {
            startEndTimesList = taskParam.getStartEndTime().split(";");
            if (ObjectUtil.isEmpty(startEndTimesList)) {
                // 校验计划结束时间是否为空
                if (ObjectUtil.isEmpty(startEndTimesList[1])) {
                    throw new ServiceException(7, "计划结束时间为空");
                }
                // 校验为计划开始是否为空
                if (ObjectUtil.isEmpty(startEndTimesList[0])) {
                    throw new ServiceException(8, "计划开始时间为空");
                }
            }
        }
        //校验编码是否为空
        if (ObjectUtil.isEmpty(taskParam.getCode())) {
            throw new ServiceException(9, "编码为空");
        }

        // 校验状态是否为空
        if (ObjectUtil.isEmpty(taskParam.getStatus())) {
            throw new ServiceException(10, "状态为空");
        }
        if (ObjectUtil.isEmpty(taskParam.getPlaNum()) || taskParam.getPlaNum() < 0) {
            throw new ServiceException(11, "计划数不能小于零，请重新输入");

        }
        LambdaQueryWrapper<Task> queryWrapperByCode = new LambdaQueryWrapper<>();
        //  String name = taskParam.getName();
        String code = taskParam.getCode();
        //校验排除
        if (isExcludeSelf) {
            Long id = taskParam.getId();
            queryWrapperByCode.ne(Task::getId, id);
        }
        queryWrapperByCode.eq(Task::getCode, code);
        int countByCode = this.count(queryWrapperByCode);
        if (countByCode >= 1) {
            throw new ServiceException(TaskExceptionEnum.ORG_CODE_REPEAT);
        }
    }
}


