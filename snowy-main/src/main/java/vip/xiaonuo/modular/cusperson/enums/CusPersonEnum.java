package vip.xiaonuo.modular.cusperson.enums;

import lombok.Getter;

/**
 * @author 87761
 */
@Getter
public enum CusPersonEnum {
    /**
     * 未开始
     */
    MAIN_CONTACTS(1,"主联系人"),
    /**
     * 执行中
     */
    NO_MAIN_CONTACTS(-1, "非主联系人");



    private final Integer code;

    private final String message;

    CusPersonEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
