package vip.xiaonuo.modular.promodel.result;

import lombok.Data;
import vip.xiaonuo.modular.promodel.entity.ProModel;

@Data
public class ProModelResult extends ProModel {
    /**
     * 产品名称
     */
    private String proName;

}
