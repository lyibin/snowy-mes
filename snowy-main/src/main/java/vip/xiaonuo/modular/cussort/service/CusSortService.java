/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.cussort.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.core.pojo.node.AntdBaseTreeNode;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.cussort.entity.CusSort;
import vip.xiaonuo.modular.cussort.param.CusSortParam;
import vip.xiaonuo.modular.protype.param.ProTypeParam;

import java.util.List;

/**
 * 客户分类service接口
 *
 * @author czw
 * @date 2022-07-20 10:24:38
 */
public interface CusSortService extends IService<CusSort> {

    /**
     * 查询客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    PageResult<CusSort> page(CusSortParam cusSortParam);

    /**
     * 客户分类列表
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    List<CusSort> list(CusSortParam cusSortParam);

    /**
     * 添加客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    void add(CusSortParam cusSortParam);

    /**
     * 删除客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    void delete(List<CusSortParam> cusSortParamList);

    /**
     * 编辑客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    void edit(CusSortParam cusSortParam);

    /**
     * 查看客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
     CusSort detail(CusSortParam cusSortParam);

    /**
     * 导出客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
     void export(CusSortParam cusSortParam);
    /**
     * 获取组织机构树
     *
     * @author czw
     * @date 2020/7/21 11:55
     */
    List<AntdBaseTreeNode> tree(CusSortParam cusSortParam);

    /**
     * 根据节点id获取所有子节点id集合
     *
     * @author xuyuxiang
     * @date 2020/3/26 11:31
     */
    List<Long> getChildIdListById(Long id);

}
